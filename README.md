# django-newsmaillist
Django-based Mail List of News
========================================================================================================================

The goal of this project just offers some simple features that less than the Mailchimp.
Take one step back to say: why not just use the Mailchimp, but rebuild another simple and less feature wheel?

First of all, the Mailchimp could not support zh-Hant web page, although in the future, I certainly bet they will.
To my target customers, the kitchen of he/she on fire and I have fire extinguishers,
it is no necessary to wait for the fire brigade.

Second, we already have some good sending mail solutions in Taiwan, for ex: newsleopard.com.
Why do I build the another similar service?
Reason one, they don't have a fancy e-paper builder;
The second reason, they only serve services around sending email,
but my customers want total solutions to solve the demands of their ec-site and CMS.
That is why I build this site as a django-based app, it will be integrated to our other web site project.

Customize E-Paper
------------------------------------------------------------------------------------------------------------------------

News Group
------------------------------------------------------------------------------------------------------------------------

Scheduled Sending
------------------------------------------------------------------------------------------------------------------------

Permission Marketing
------------------------------------------------------------------------------------------------------------------------

Multi-Sites Control
------------------------------------------------------------------------------------------------------------------------

Api Control
------------------------------------------------------------------------------------------------------------------------

Reference
========================================================================================================================

** Multi messengers, Reusable application for Django introducing a message delivery framework **: https://github.com/idlesign/django-sitemessage

mail queue backend: https://github.com/Khabi/django-mcfeely

Mail Queue: https://github.com/dstegelman/django-mail-queue

multi email backends: https://github.com/tbarbugli/django_email_multibackend

unsubscribe: https://github.com/theskumar/django-unsubscribe

speed-up HTML email developpement workflow in django: https://github.com/EliotBerriot/django-dbes

Django Mail Factory lets you manage your email in a multilingual project: https://github.com/novafloss/django-mail-factory/

send PGP encrypted and multipart emails using Django templates: https://github.com/stephenmcd/django-email-extras

A Django email backend that uses a Celery queue for out-of-band sending of the messages: https://github.com/pmclanahan/django-celery-email

Class-based email views for the Django framework, including a message previewer: https://github.com/disqus/django-mailviews

Django Post Office is a simple app to send and manage your emails in Django: https://github.com/ui/django-post_office

* Allows you to send email asynchronously
* Multi backend support
* Supports HTML email
* Supports database based email templates
* Built in scheduling support
* Works well with task queues like RQ or Celery
* Uses multiprocessing to send a large number of emails in parallel
* Supports multilingual email templates (i18n)

mail queuing and management for the Django web framework: https://github.com/pinax/django-mailer

A Django email backend for Amazon's Simple Email Service: https://github.com/django-ses/django-ses